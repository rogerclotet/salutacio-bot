#!/bin/sh

(
  sshpass -p $SSH_PASSWORD ssh $SSH_USERNAME@$SSH_IP -o StrictHostKeyChecking=no <<-EOF
    cd $SSH_PROJECT_FOLDER
    git pull
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml pull
    docker-compose -f docker-compose.yml -f docker-compose.prod.yml stop
    TELEGRAM_API_TOKEN=$TELEGRAM_API_TOKEN RESOURCES_PATH=$RESOURCES_PATH DEBUG=$DEBUG \
        docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
EOF
)
