package main

import (
	"bufio"
	"fmt"
	"log"
	"math/rand"
	"os"
	"path/filepath"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func loadWords(path string) []string {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	words := []string{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		word := strings.TrimSpace(scanner.Text())
		if word != "" {
			words = append(words, word)
		}
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	return words
}

func newGreeter(resourcesPath string, stats *statsStore) *greeter {
	var intros = loadWords(filepath.Join(resourcesPath, "intros.txt"))
	var names = loadWords(filepath.Join(resourcesPath, "names.txt"))

	log.Printf("Loaded %d intros and %d names", len(intros), len(names))

	return &greeter{
		intros: intros,
		names:  names,
		stats:  stats,
	}
}

type greeter struct {
	intros []string
	names  []string
	stats  *statsStore
}

func (g *greeter) greeting(user *tgbotapi.User) string {
	g.stats.addGreeting(user)
	intro := g.intros[rand.Intn(len(g.intros))]
	name := g.names[rand.Intn(len(g.names))]
	return fmt.Sprintf("%s %s! 👋", intro, name)
}
