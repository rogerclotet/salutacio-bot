# SalutacioBot for Telegram

You can use it by talking to [@SalutacioBot](https://t.me/SalutacioBot) or running it by following the next steps:

## How to run the bot

### Locally

1. Create a bot as [explained in the telegram documentation](https://core.telegram.org/bots)
2. Enable the inline mode in your newly created bot by sending `/setinline` to [@BotFather](https://t.me/BotFather)
3. Run the bot with `TELEGRAM_API_TOKEN=<yourToken> RESOURCES_PATH=<absolute path to ./resources> REDIS_ADDR=localhost:6379 go run .`

### With docker-compose

1. Create a bot as [explained in the telegram documentation](https://core.telegram.org/bots)
2. Enable the inline mode in your newly created bot by sending `/setinline` to [@BotFather](https://t.me/BotFather)
3. Set the token you got when creating the bot in .env.local
4. Run the bot with `docker-compose`

Messages in console should indicate the account was authorized and you should be able to invoke your bot in any chat by typing its name in the text box.

## Commands

There are two commands that can be enabled for your bot using `/setcommands` with [@BotFather](https://t.me/BotFather):

### `/stats`

Get stats about the bot's usage. They are stored in redis, using persistence by default if you use `docker-compose`.

### `/about`

Get information about the bot.
