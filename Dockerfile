FROM golang:1.14

ADD . /code
WORKDIR /code

RUN go install .

ENTRYPOINT /go/bin/salutacio-bot
