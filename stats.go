package main

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"

	"github.com/go-redis/redis/v7"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func newRedisStatsStore(addr string) *statsStore {
	return &statsStore{
		client: redis.NewClient(&redis.Options{Addr: addr}),
	}
}

type statsStore struct {
	client *redis.Client
}

const greetingsKey = "greetings"
const countKey = "count"
const lastUserKey = "lastUser"

func userKey(userID int) string {
	return fmt.Sprintf("user_%d", userID)
}

func dayGreetingsKey(t time.Time) string {
	y, m, d := t.Date()
	return fmt.Sprintf("daily_greetings_%d-%d-%d", d, m, y)
}

func userInfo(user *tgbotapi.User) string {
	nameParts := []string{}
	if user.FirstName != "" {
		nameParts = append(nameParts, user.FirstName)
	}
	if user.LastName != "" {
		nameParts = append(nameParts, user.LastName)
	}
	if user.UserName != "" {
		nameParts = append(nameParts, fmt.Sprintf("@%s", user.UserName))
	}
	return strings.Join(nameParts, " ")
}

func (s *statsStore) addGreeting(user *tgbotapi.User) {
	userID := strconv.Itoa(user.ID)
	userGreetings := s.client.ZScore(greetingsKey, userID).Val()
	isNewUser := userGreetings == 0
	if isNewUser {
		s.client.Set(lastUserKey, userID, 0)
	}

	s.client.ZIncrBy(greetingsKey, 1, userID)

	t := time.Now()
	dayKey := dayGreetingsKey(t)
	s.client.ZIncrBy(dayKey, 1, userID)
	s.client.Expire(dayKey, 24*time.Hour)

	s.client.IncrBy(countKey, 1)
	s.client.Set(userKey(user.ID), userInfo(user), 0)
}

func (s *statsStore) greetingsRanking(greetings []redis.Z) []string {
	lines := []string{}
	for i, greeting := range greetings {
		userID, err := strconv.Atoi(greeting.Member.(string))
		if err != nil {
			log.Printf("could not convert user id to int: %s", err)
			continue
		}
		userInfo, err := s.client.Get(userKey(userID)).Result()
		if err != nil {
			log.Printf("could not get user info from user id: %s", err)
			continue
		}
		lines = append(lines, fmt.Sprintf("  %d. %s: %d", i+1, userInfo, int(greeting.Score)))
	}
	return lines
}

func (s *statsStore) lastUser() []string {
	lastUser, err := s.client.Get(lastUserKey).Result()
	if err != nil || lastUser == "" {
		return []string{}
	}
	userID, err := strconv.Atoi(lastUser)
	if err != nil {
		log.Printf("could not convert user id to int: %s", err)
		return []string{}
	}
	userInfo, err := s.client.Get(userKey(userID)).Result()
	if err != nil {
		log.Printf("could not get user info from user id: %s", err)
		return []string{}
	}
	return []string{fmt.Sprintf("*Últim usuari:* %s", userInfo)}
}

func (s *statsStore) String() string {
	lines := []string{"*Estadístiques*"}

	countStr, err := s.client.Get(countKey).Result()
	if err != nil {
		return fmt.Sprintf("could not get greetings count: %s", err)
	}
	count, err := strconv.Atoi(countStr)
	if err != nil {
		count = 0
	}
	lines = append(lines, fmt.Sprintf("%d salutacions", count))

	usersCount, err := s.client.ZCard(greetingsKey).Result()
	if err != nil {
		return fmt.Sprintf("could not get users count: %s", err)
	}
	lines = append(lines, fmt.Sprintf("%d usuaris", usersCount))

	lines = append(lines, s.lastUser()...)

	todayKey := dayGreetingsKey(time.Now())
	if todayGreetings, err := s.client.ZRevRangeWithScores(todayKey, 0, 9).Result(); err == nil && len(todayGreetings) > 0 {
		lines = append(lines, fmt.Sprintf("*Salutacions avui:*"))
		lines = append(lines, s.greetingsRanking(todayGreetings)...)
	}

	if greetings, err := s.client.ZRevRangeWithScores(greetingsKey, 0, 9).Result(); err == nil && len(greetings) > 0 {
		lines = append(lines, fmt.Sprintf("*Salutacions totals:*"))
		lines = append(lines, s.greetingsRanking(greetings)...)
	}

	return strings.Join(lines, "\n")
}
