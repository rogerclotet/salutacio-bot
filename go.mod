module gitlab.com/rogerclotet/salutacio-bot

go 1.14

require (
	github.com/go-redis/redis/v7 v7.2.0
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
