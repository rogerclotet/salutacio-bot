package main

import (
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	token := os.Getenv("TELEGRAM_API_TOKEN")
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		log.Panic(err)
	}

	debug := os.Getenv("DEBUG") == "true"
	log.Printf("Using debug %v", debug)

	bot.Debug = debug

	log.Printf("Authorized on account %s", bot.Self.UserName)

	redisAddr := os.Getenv("REDIS_ADDR")
	stats := newRedisStatsStore(redisAddr)

	resourcesPath := os.Getenv("RESOURCES_PATH")
	g := newGreeter(resourcesPath, stats)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.InlineQuery != nil {
			greeting := g.greeting(update.InlineQuery.From)
			inlineResult := tgbotapi.NewInlineQueryResultArticle(update.InlineQuery.ID, "Enviar salutació", greeting)

			bot.AnswerInlineQuery(tgbotapi.InlineConfig{
				InlineQueryID: update.InlineQuery.ID,
				Results:       []interface{}{inlineResult},
			})
		}

		if update.Message != nil {
			if update.Message.IsCommand() {
				switch update.Message.Command() {
				case "stats":
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, stats.String())
					msg.ParseMode = "markdown"
					bot.Send(msg)
				case "about":
					lines := []string{
						"*Sobre aquest bot*",
						"Aquest bot és de codi obert, i es pot modificar i utilitzar lliurement.",
						"Repositori: https://gitlab.com/rogerclotet/salutacio-bot",
						"Autor: Roger Clotet - @clotet - https://clotet.dev",
					}
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, strings.Join(lines, "\n"))
					msg.ParseMode = "markdown"
					bot.Send(msg)
				}
			}
		}
	}
}
